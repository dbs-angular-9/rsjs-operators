const { ReplaySubject } = require( 'rxjs');

const subject = new ReplaySubject(4); // buffer 3 values for new subscribers

const handle2 = subject.subscribe({
  next: (v) => console.log(`observerA: ${v}`)
});



subject.next(1);
subject.next(2);
subject.next(3);
subject.next(4);
handle2.unsubscribe()
subject.next(5);
subject.next(6);
subject.closed();
//subject.next(7);

const handle = subject.subscribe({
  next: (v) => console.log(`observerB: ${v}`)
});

subject.next(5);